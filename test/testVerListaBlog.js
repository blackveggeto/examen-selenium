const { By, Key, Builder, until } = require("selenium-webdriver");
require("chromedriver");

async function test_case() {
    let driver = await new Builder().forBrowser("chrome").build();
    //Para vists Admin galeria
    await driver.get("http://localhost:3000/vistaAdminBlog");

    //Para agrandar la lista #8
    /*let select = await driver.findElement(By.name('example_length'));
    await select.click();

    let options = await select.findElements(By.tagName('option'));
    for (let option of options) {
        if (await option.getAttribute('value') === '25') {
            await option.click();
            break;
        }
    }*/

    let select = await driver.wait(until.elementLocated(By.name('vistaBlog_length')),4000);
    await select.click();

    let options = await select.findElements(By.tagName('option'));
    for (let option of options) {
        if (await option.getAttribute('value') === '25') {
            await option.click();
            break;
        }
    }
    

    setInterval(function () {
        driver.quit();
    }, 5000);

}

test_case();