const { By, Key, Builder, until } = require("selenium-webdriver");
require("chromedriver");

async function test_case() {
    let driver = await new Builder().forBrowser("chrome").build();
    //Para vists Admin blog
    await driver.get("http://localhost:3000/vistaAdminBlog");
    /*Si sirve para escribir
    const title = await driver.wait(until.elementLocated(By.name("title")),5000);
    await title.sendKeys("Texto de ejemplo");*/

    //Para ir a ver un blog #1
    let link_blog = await driver.wait(until.elementLocated(By.css(`[class='fa fa-eye']`)), 5000);
    await link_blog.click();


    /*const pageSource = await driver.getPageSource();
    const wordExists = pageSource.includes(`viaje para una playa`);
   console.log(wordExists);*/

    const elements = await driver.findElements(By.xpath("//*[contains(text(), 'playa')]"));
    const wordExists = elements.length > 0;
    console.log(wordExists);


}

test_case();