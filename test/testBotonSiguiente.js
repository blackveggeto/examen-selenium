const { By, Key, Builder, until } = require("selenium-webdriver");
require("chromedriver");

async function test_case() {
    let driver = await new Builder().forBrowser("chrome").build();
    //Para vists Admin galeria
    await driver.get("http://localhost:3000/vistaAdmingaleria");

    //Para darle boton siguiente #6
    let link_next = await driver.wait( until.elementLocated(By.css('.paginate_button.next')),4000);
    await link_next.click();

}

test_case();