const { By, Key, Builder, until } = require("selenium-webdriver");
require("chromedriver");

async function test_case() {
    let driver = await new Builder().forBrowser("chrome").build();
    //Para vists Admin blog
    await driver.get("http://localhost:3000/vistaAdminBlog");


    //Para editar un blog #2
    let link_edit = await driver.wait(until.elementLocated(By.css(`[class='fa fa-edit']`)), 5000);
    await link_edit.click();
    const title = await driver.wait(until.elementLocated(By.name("title")),5000);
    await title.sendKeys("Texto de ejemplo");
    
    setInterval(function () {
        driver.quit();
    }, 5000);

}

test_case();
