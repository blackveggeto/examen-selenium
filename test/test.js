const { By, Key, Builder, until } = require("selenium-webdriver");
require("chromedriver");

async function test_case() {
    let driver = await new Builder().forBrowser("chrome").build();
    //Para vists Admin galeria
    await driver.get("http://localhost:3000/vistaAdmingaleria");

    /*//Para buscar texto #1
    let input = await driver.findElement(By.css(`input[type='search']`));
    await input.sendKeys('Aguila');*/

    /*//prueba de ver esta lista #2
    let element_eye = await driver.findElement(By.css(`button[type='button'][class='fa fa-eye']`));
    await element_eye.click();*/

    /*//prueba de borrar #3
    let element_trash = await driver.findElement(By.css(`button[type='button'][class='fa fa-trash-o']`));
    await element_trash.click();*/

   /* //para eliminar #4
    let element_trash2 = await driver.findElement(By.css(`button.swal-button.swal-button--confirm.swal-button--danger`));
    await element_trash2.click();*/

    /*//Para cancelar eliminar #5
    let element_trash3 = await driver.findElement(By.css(`button.swal-button.swal-button--cancel`));
    await element_trash3.click();*/

    /*//Para darle boton siguiente #6
    let link_next = await driver.findElement(By.css('.paginate_button.next'));
    await link_next.click();*/

    /*//Para regresar #7
    let link_back = await driver.findElement(By.css('.paginate_button.previous'));
    await link_back.click();*/

    //Para agrandar la lista #8
    let select = await driver.findElement(By.name('example_length'));
    await select.click();

    let options = await select.findElements(By.tagName('option'));
    for (let option of options) {
        if (await option.getAttribute('value') === '25') {
            await option.click();
            break;
        }
    }

    setInterval(function () {
        driver.quit();
    }, 5000);

}

test_case();